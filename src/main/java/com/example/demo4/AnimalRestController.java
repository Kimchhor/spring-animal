package com.example.demo4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping
public class AnimalRestController {
    @Autowired
    private AnimalRepository animalRepository;

    public AnimalRestController(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    @GetMapping("/")
    public List<AnimalModel> getAll(){
        return animalRepository.findAll();
    }
}
