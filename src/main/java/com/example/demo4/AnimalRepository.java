package com.example.demo4;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AnimalRepository {
    @Select("SELECT * FROM animal")
    public List<AnimalModel> findAll();
}
